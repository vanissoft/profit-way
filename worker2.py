"""
WORKER 2

Tasks: SimpleBot

"""

import trio
import json
from collections import namedtuple
from broker import Broker_client, Tasks
from models import Settings
import utils
from manualSIGNING import it
import manualSIGNING as ms
from time import time

Mem = utils.Borg()

Order = namedtuple('order', ['type', 'base_amount', 'base_asset', 'quote_amount', 'quote_asset', 'price'])



class AccountOrders():
	def __init__(self, parent):
		self.broker = parent.broker
		self.assets = parent.assets
		self.asset_list = set()
		self.order_list = []

	async def start(self, params, cb):
		def account_details(rtn):
			for l in rtn['result'][0][1]['limit_orders']:
				if l['sell_price']['base']['asset_id'] in self.assets.assets and l['sell_price']['quote']['asset_id'] in self.assets.assets:
					self.order_list.append(utils.limit_order(l, self.assets))
			print(it('cyan', 'account orders complete'))
			cb(True)

		self.order_list = []
		await self.broker.query('bts_query', ['database', 'get_full_accounts', [[params[0]], False]], account_details)



class Balances():
	def __init__(self, parent):
		self.broker = parent.broker
		self.assets = parent.assets
		self.asset_list = set()
		self.balances = {}
		self.order_list = []
		self.balances_done = False


	async def start(self, params, cb):
		def balances(rtn):
			for b in rtn['result']:
				self.balances[self.assets.assets_name[b['asset_id']]] = b['amount'] / 10 ** self.assets.assets[b['asset_id']].precision
			print(it('cyan', 'balances complete'))
			self.balances_done = True

		self.order_list = []
		self.balances_done = False
		await self.broker.query('bts_query', ['database', 'get_account_balances', [params[0], [x for x in self.assets.assets.keys()]]], balances)
		#TODO: sometimes assets.pending hangs forever
		while not self.balances_done or self.assets.pending():
			await trio.sleep(5)
		print("complete")
		#await self.assets.check_asset(asset_id='1.3.0')
		#while self.num_responses < len(self.markets):
		#	await trio.sleep(1)
		cb(True)


class Bts_order():

	def __init__(self, parent):
		self.parent = parent
		self.assets = parent.assets
		self.block_num = None
		self.tx = {}

	async def put(self, orders):
		def block_number(rtn):
			self.block = rtn['result']
			orders_tx = []
			for o in orders:
				to_sell = int(o['amount'] * 10 ** self.assets.assets[self.assets.assets_id[o['asset_name']]].precision)
				to_receive = int(o['amount'] * (1/o['price']) * 10 ** self.assets.assets[self.assets.assets_id[o['asset2_name']]].precision)
				orders_tx.append({'expires': o['expiration']*60, 'to_sell': [self.assets.assets_id[o['asset_name']], to_sell],
											   'to_receive': [self.assets.assets_id[o['asset2_name']], to_receive]})
			tx = ms.create_tx(orders_tx, Mem.settings.fees, self.block, orders_to_cancel=[])
			self.tx['tx'] = tx
			self.parent.broker.query_sync('bts_query', ["database", "get_transaction_hex_without_sig", [tx]], tx_hex)
		def tx_hex(rtn):
			self.tx['tx_hex'] = rtn['result']
			sign_and_send()

		def sign_and_send():
			def broadcast(rtn):
				self.cancel_orders = []
				print(rtn)

			print(ms.it('yellow', "createorder2"))
			tx, message = ms.serialize_transaction(self.tx['tx'], self.tx['tx_hex'])
			signed_tx = ms.sign_transaction(tx, message, Mem.settings.wif.get_secret_value())
			if False:
				verified_tx = ms.verify_transaction(signed_tx, Mem.settings.wif.get_secret_value())
				assert signed_tx == verified_tx
			self.parent.broker.query_sync('bts_query', ["network_broadcast", "broadcast_transaction", [signed_tx]], broadcast)

		print(ms.it('yellow', "createorder1"))
		await self.parent.broker.query('bts_query', ['database', 'get_dynamic_global_properties', []], block_number)





class SimpleBot():
	def __init__(self, parent):
		self.parent = parent
		self.broker = parent.broker
		self.assets = parent.assets
		self.orders_to_get = 3
		self.g = None
		self.markets = None
		self.orders = []
		self.mins = {}  # minimal trades for each market


	async def start(self, params, cb):
		self.balances = self.parent.task_broker.tasks['get_balances'].balances
		self.order_list = self.parent.task_broker.tasks['get_balances'].order_list
		print("simplebot job", params)
		self.mins = {(x['markets'][n[0]], x['markets'][n[1]]): (x['amount'][n[0]]/x['orders'], x['amount'][n[1]]/x['orders']) for x in Mem.bot_instances for n in [[0,1], [1,0]]}
		self.markets, _ = utils.read_orderbook_file(filename=params[0], assets=None, depth=4, minimums=self.mins)
		self.orders = []
		if not self.balances:
			print(it('red', 'balance not completed, wait next cycle'))
			return
		balance = {x[0]:x[1] for x in self.balances.items()}
		#TODO: insufficient balance errors
		print(balance)
		for ins in Mem.bot_instances:
			if 'state' in ins and ins['state'] == 0:  # state==0 == stoped
				continue
			for n in (0,1):
				if ins['markets'][n] not in self.balances:
					continue
				print("balances", ins['markets'][n], self.balances[ins['markets'][n]], ins['amount'][n] / (ins['orders']-1))
				if self.balances[ins['markets'][n]] > ins['amount'][n] / (ins['orders']-1):  # ready to work with
					asset_name = ins['markets'][n]
					asset2_name = ins['markets'][abs(n-1)]
					if (asset2_name, asset_name) not in self.markets:
						continue
					if 'bias' in ins:
						if asset_name == ins['markets'][0]:
							price_bias = ins['bias'][0]
						else:
							price_bias = ins['bias'][1]
					else:
						price_bias = 1

					_, _, price1, price2 = self.markets[(asset2_name, asset_name)]
					for p in reversed(utils.price_range(price1, price2, ins['orders'])):
						p *= price_bias
						amount = ins['amount'][n] / ins['orders']
						balance[ins['markets'][n]] -= amount
						if balance[ins['markets'][n]] <= 0:
							break
						self.orders.append({'amount': amount, 'asset_name': ins['markets'][n],
											'asset2_name': asset2_name, 'price': p,
											'expiration': ins['expiration']})
		if self.orders:
			self.orders.sort(key=lambda x: x['asset_name'])
			slist = []
			s1 = None
			while True:
				if self.orders:
					s = self.orders.pop(0)
				if not self.orders or (s1 and s1['asset_name'] != s['asset_name']):
					await Bts_order(parent=self).put(slist)
					slist = []
				if not self.orders:
					break
				s1 = s
				slist.append(s)

		cb(True)


class Logic():

	def __init__(self, task_broker):
		self.task_broker = task_broker
		self.broker = task_broker.broker
		self.assets = utils.Assets(broker=self.broker)
		self.task_broker.tasks = {'simplebot': SimpleBot(parent=self),
								  'get_balances': Balances(parent=self),
								  'account_orders': AccountOrders(parent=self)
								  }
		self.bot_markets = []
		self.cycle = 60*4
		self.last_orderbook = 0
		self.last_balances = 0
		self.init_done = False


	def launch_simplebot(self):
		def end_bot_job(rtn):
			print("done botcycle", rtn)
		if time() - self.last_orderbook < self.cycle/2:
			self.broker.query_sync('enqueue simplebot', ['orderbook.csv'], end_bot_job)

	def orderbook_ready(self, rtn):
		print(it('purple', rtn))
		if 'op' in rtn and rtn['op'] == 'taskdone':
			self.last_orderbook = time()
			self.launch_simplebot()

	def balances_ready(self, rtn):
		print(it('purple', rtn))
		if 'op' in rtn and rtn['op'] == 'taskdone':
			self.last_balances = time()
			self.launch_simplebot()


	async def init(self):

		await self.broker.query('subscribe get_bts_orderbook', [], self.orderbook_ready)
		await self.broker.query('subscribe get_balances', [], self.balances_ready)

		# bias: multiplier to price. > 1 advance price  < 1 reduce price
		Mem.bot_instances = [
			{'state': 1, 'markets': ('BRIDGE.BTC', 'BTS'), 'amount': (0.002, 303), 'orders': 3, 'expiration': 10, 'bias': (1.001,0.92), 'output': 'simplebot3.csv'},
			{'state': 1, 'markets': ('BRIDGE.USDT', 'BTS'), 'amount': (12, 303), 'orders': 3, 'expiration': 15, 'bias': (1.001, 0.91), 'output': 'simplebot3.csv'},
			{'state': 1, 'markets': ('SPARKDEX.BTC', 'BTS'), 'amount': (0.002, 303), 'orders': 3, 'expiration': 10, 'bias': (0.99,0.96), 'output': 'simplebot3.csv'},
			{'state': 1, 'markets': ('GDEX.EOS', 'BTS'), 'amount': (.14, 200), 'orders': 3, 'expiration': 15, 'bias': (1.001,1.001), 'output': 'simplebot1.csv'},
			{'state': 1, 'markets': ('ZEPH', 'BTS'), 'amount': (4000, 300), 'orders': 3, 'expiration': 15, 'bias': (0.94,0.98), 'output': 'simplebot3.csv'},
			{'state': 1, 'markets': ('SPARKDEX.HKD', 'BTS'), 'amount': (70, 300), 'orders': 3, 'expiration': 15, 'bias': (1,1), 'output': 'simplebot3.csv'},
			{'state': 1, 'markets': ('GDEX.EOS', 'RUDEX.EOS'), 'amount': (.5, .5), 'orders': 3, 'expiration': 15, 'output': 'simplebot2.csv'},
			{'state': 1, 'markets': ('CNY', 'EUR'), 'amount': (10, 0.5), 'expiration': 15, 'orders': 3, 'output': 'simplebot4.csv'}
							 ]

		def fees(rtn):
			Mem.settings.fees = {'create': rtn['result'][0]['amount'], 'cancel': rtn['result'][1]['amount']}
		def account(rtn):
			if rtn['result'] == [None]:
				print("account unknown")
			else:
				Mem.settings.account_id = rtn['result'][0]['id']
				self.broker.query_sync('bts_query', ["database", "get_required_fees", [[["1", {"from": Mem.settings.account_id}], ["2", {"from": Mem.settings.account_id}]], "1.3.0"]], fees)
			self.init_done = True
		self.bot_markets = [x['markets'] for x in Mem.bot_instances]
		for pair in self.bot_markets:
			self.assets.check_asset(asset_name=pair[0])
			self.assets.check_asset(asset_name=pair[1])
		await self.broker.query('bts_query', ['database', 'get_accounts', [[Mem.settings.account.get_secret_value()]]], account)


	async def bot_cycle(self):
		def end_get_balances(rtn):
			print("done balances", rtn)
		def end_get_orders(rtn):
			print("done account orders", rtn)
		if time() - self.last_orderbook > self.cycle/2 or self.last_orderbook == 0:
			await self.broker.query('enqueue get_bts_orderbook', ['orderbook.csv', self.bot_markets], self.orderbook_ready)
		await self.broker.query('enqueue account_orders', [Mem.settings.account_id], end_get_orders)
		await self.broker.query('enqueue get_balances', [Mem.settings.account_id], end_get_balances)


	async def main(self):
		await self.init()
		while not self.init_done:
			await trio.sleep(1)
		while True:
			await self.bot_cycle()
			await trio.sleep(self.cycle)

	async def start(self):
		async with trio.open_nursery() as nur:
			nur.start_soon(self.task_broker.start)
			nur.start_soon(self.main)



def load_settings():
	import glob
	cfg_file = '.simplebot1.cfg'
	st = Settings(account='account', account_id='', wif='234324324', markets=['a', 'b'])
	if len(glob.glob(cfg_file)) == 0:
		with open(cfg_file, 'w') as f:
			f.write(st.json())
	else:
		with open(cfg_file, 'r') as f:
			txt = f.read()
		Mem.settings = Settings(**json.loads(txt))



if __name__ == '__main__':
	load_settings()
	main = Logic(task_broker=Tasks(broker=Broker_client()))
	trio.run(main.start)

