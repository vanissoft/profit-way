Work in progress!!!

Proof of concept of async operations against BitShares nodes.

# Instructions
1. Update .config.cfg, .worker1.cfg, .worker2.cfg with account name
    1. If you want to put real orders, put the correspondent wif into the .cfg files.
1. Start main.py and worker1.py.
1. Results are in orderbook.csv and profit_path.txt.
    1. Bot doesn't generates any file, only put real orders on blockchain.
