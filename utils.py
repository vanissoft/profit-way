

import pickle
import models
from collections import namedtuple
import networkx as nx


Node_list = ["wss://altcap.io/wss",
        "wss://api-ru.bts.blckchnd.com/ws",
        "wss://api.bitshares.bhuz.info/wss",
        "wss://api.bitsharesdex.com/ws",
        "wss://api.bts.ai/ws",
        "wss://api.bts.blckchnd.com/wss",
        "wss://api.bts.mobi/wss",
        "wss://api.bts.network/wss",
        "wss://api.btsgo.net/ws",
        "wss://api.btsxchng.com/wss",
        "wss://api.dex.trading/ws",
        "wss://api.fr.bitsharesdex.com/ws",
        "wss://api.open-asset.tech/wss",
        "wss://atlanta.bitshares.apasia.tech/wss",
        "wss://australia.bitshares.apasia.tech/ws",
        "wss://bit.btsabc.org/wss",
        "wss://bitshares.crypto.fans/wss",
        "wss://bitshares.cyberit.io/ws",
        "wss://bitshares.dacplay.org/wss",
        "wss://bitshares.dacplay.org:8089/wss",
        "wss://bitshares.openledger.info/wss",
        "wss://blockzms.xyz/ws",
        "wss://bts-api.lafona.net/ws",
        "wss://bts-seoul.clockwork.gr/ws",
        "wss://bts.liuye.tech:4443/wss",
        "wss://bts.open.icowallet.net/ws",
        "wss://bts.proxyhosts.info/wss",
        "wss://btsfullnode.bangzi.info/ws",
        "wss://btsws.roelandp.nl/ws",
        "wss://chicago.bitshares.apasia.tech/ws",
        "wss://citadel.li/node/wss",
        "wss://crazybit.online/wss",
        "wss://dallas.bitshares.apasia.tech/wss",
        "wss://dex.iobanker.com:9090/wss",
        "wss://dex.rnglab.org/ws",
        "wss://dexnode.net/ws",
        "wss://england.bitshares.apasia.tech/ws",
        "wss://eu-central-1.bts.crypto-bridge.org/wss",
        "wss://eu.nodes.bitshares.ws/ws",
        "wss://eu.openledger.info/ws",
        "wss://france.bitshares.apasia.tech/ws",
        "wss://frankfurt8.daostreet.com/wss",
        "wss://japan.bitshares.apasia.tech/wss",
        "wss://kc-us-dex.xeldal.com/ws",
        "wss://kimziv.com/ws",
        "wss://la.dexnode.net/ws",
        "wss://miami.bitshares.apasia.tech/ws",
        "wss://na.openledger.info/ws",
        "wss://ncali5.daostreet.com/wss",
        "wss://netherlands.bitshares.apasia.tech/ws",
        "wss://new-york.bitshares.apasia.tech/ws",
        "wss://node.bitshares.eu/ws",
        "wss://node.market.rudex.org/wss",
        "wss://openledger.hk/wss",
        "wss://paris7.daostreet.com/wss",
        "wss://relinked.com/wss",
        "wss://scali10.daostreet.com/wss",
        "wss://seattle.bitshares.apasia.tech/wss",
        "wss://sg.nodes.bitshares.ws/ws",
        "wss://singapore.bitshares.apasia.tech/ws",
        "wss://status200.bitshares.apasia.tech/wss",
        "wss://us-east-1.bts.crypto-bridge.org/ws",
        "wss://us-la.bitshares.apasia.tech/ws",
        "wss://us-ny.bitshares.apasia.tech/ws",
        "wss://us.nodes.bitshares.ws/wss",
        "wss://valley.bitshares.apasia.tech/ws",
        "wss://ws.gdex.io/ws",
        "wss://ws.gdex.top/wss",
        "wss://ws.hellobts.com/wss",
        "wss://ws.winex.pro/wss",
		 "wss://altcap.io",
		 "wss://ap-northeast-1.bts.crypto-bridge.org/wss",
		 "wss://ap-southeast-1.bts.crypto-bridge.org/wss",
		 "wss://ap-southeast-2.bts.crypto-bridge.org",
		 "wss://ap-southeast-3.bts.crypto-bridge.org/ws",
		 "wss://api.bitsharesdex.com/wss",
		 "wss://api.bts.ai/ws",
		 "wss://api.bts.mobi/wss",
		 "wss://api.bts.network/ws",
		 "wss://api.dex.trading",
		 "wss://api.fr.bitsharesdex.com/ws",
		 "wss://b.mrx.im/wss",
		 "wss://bitshares.cyberit.io/ws",
		 "wss://bitshares.openledger.info/wss",
		 "wss://bts-api.lafona.net/wss",
		 "wss://bts-seoul.clockwork.gr/wss",
		 "wss://bts.liuye.tech:4443/ws",
		 "wss://btsfullnode.bangzi.info/ws",
		 "wss://btsws.roelandp.nl/wss",
		 "wss://dex.iobanker.com:9090/ws",
		 "wss://dexnode.net/ws",
		 "wss://eu-central-1.bts.crypto-bridge.org/ws",
		 "wss://eu.nodes.bitshares.ws/ws",
		 "wss://freedom.bts123.cc:15138/wss",
		 "wss://kc-us-dex.xeldal.com/wss",
		 "wss://kimziv.com/ws",
		 "wss://na.openledger.info/wss",
		 "wss://node.bitshares.eu/wss",
		 "wss://nohistory.proxyhosts.info/wss",
		 "wss://openledger.hk/wss",
		 "wss://sg.nodes.bitshares.ws/wss",
		 "wss://us-west-2.bts.crypto-bridge.org/ws",
		 "wss://us.nodes.bitshares.ws",
		 "wss://ws.gdex.io/wss",
		 "wss://ws.gdex.top/ws",
			 ]

class Borg(object):
	__shared_state = {}

	def __init__(self):
		self.__dict__ = self.__shared_state

	def save(self):
		with open('data.pickle', 'wb') as f:
			pickle.dump(self.__dict__, f)

	def load(self):
		with open('data.pickle', 'rb') as f:
			self.__dict__ = pickle.load(f)


# Code from Python docs regarding partial from functools
def partial(func, *args, **keywords):
	def newfunc(*fargs, **fkeywords):
		newkeywords = keywords.copy()
		newkeywords.update(fkeywords)
		return func(*(args + fargs), **newkeywords)

	newfunc.func = func
	newfunc.args = args
	newfunc.keywords = keywords
	return newfunc


class Assets(Borg):
	def __init__(self, broker):
		super().__init__()
		self.broker = broker
		self.assets_id = {}  # ['name'] = id
		self.assets_name = {}  # ['id'] = name
		self.assets = {}  # ['id'] = asset data
		self.assets_pending_name = []
		self.assets_pending_id = []

	def query_asset(self, asset_name=None, asset_id=None):
		def response(rtn):
			data = rtn['result'][0]
			self.assets[data['id']] = models.Asset(**data)
			self.assets_id[data['symbol']] = data['id']
			self.assets_name[data['id']] = data['symbol']
			if data['id'] in self.assets_pending_id:
				self.assets_pending_id.remove(data['id'])
			if data['symbol'] in self.assets_pending_name:
				self.assets_pending_name.remove(data['symbol'])
			print("got asset ", data['symbol'])
		print("assets query_asset1", asset_name, asset_id)
		if asset_name not in self.assets_pending_name and asset_id not in self.assets_pending_id:
			print("assets query_asset2", asset_name, asset_id)
			if asset_name:
				self.assets_pending_name.append(asset_name)
			elif asset_id:
				self.assets_pending_id.append(asset_id)
			self.broker.query_sync('bts_query', ['database', "get_assets", [[asset_name or asset_id]]], response)

	def check_asset(self, asset_name=None, asset_id=None):
		if asset_name:
			if asset_name not in self.assets_id:
				self.query_asset(asset_name=asset_name)
		elif asset_id:
			if asset_name not in self.assets_name:
				self.query_asset(asset_id=asset_id)

	def pending(self):
		return len(self.assets_pending_id)+len(self.assets_pending_name) > 0

	def get_by_id(self, asset_id):
		if asset_id not in self.assets:
			return None
		return self.assets_name[asset_id]
	def get_by_name(self, asset_name):
		if asset_name not in self.assets_id:
			return None
		return self.assets_id[asset_name]


def read_orderbook_file(filename, assets, depth=1, minimums=None):
	if assets:
		asset_list = [x[0] for x in assets]
	g = nx.Graph()
	markets = {}
	dupli = {}
	Order = namedtuple('order', ['type', 'base_amount', 'base_asset', 'quote_amount', 'quote_asset', 'price'])
	with open(filename, 'r') as f:
		while True:
			lin = f.readline().split(',')
			if len(lin) < 2:
				break
			lin = [l.strip() for l in lin]
			if lin[0] != 'SELL':
				continue
			order = Order(*lin)
			if 'POLIS' in order.base_asset:
				print()
			# ---- ignore orders below minimums and orders whose market not being in minimums list
			if minimums:
				if (order.base_asset, order.quote_asset) not in minimums:
					continue
				min = minimums[(order.base_asset, order.quote_asset)]
				if float(order.base_amount) < min[0] or float(order.quote_amount) < min[1]:
					continue
			if (order.type, order.base_asset, order.quote_asset) in dupli:
				if dupli[(order.type, order.base_asset, order.quote_asset)] > depth - 1:
					continue
				dupli[(order.type, order.base_asset, order.quote_asset)] += 1
			else:
				dupli[(order.type, order.base_asset, order.quote_asset)] = 1
			g.add_edge(order.base_asset, order.quote_asset)
			if assets:
				if order.base_asset in asset_list:
					g.add_edge(order.base_asset + '*', order.quote_asset)
				if order.quote_asset == asset_list:
					g.add_edge(order.base_asset, order.quote_asset + '*')

			# ex: in 71236 BTS  out 1 OPEN.BTC  @0.0000140
			if (order.base_asset, order.quote_asset) in markets:
				base_amount, price, extreme_price, first_price = markets[(order.base_asset, order.quote_asset)]
				markets[(order.base_asset, order.quote_asset)] = [float(order.base_amount)+base_amount,
																  ((base_amount*price)+(float(order.base_amount)*float(order.price)))/(float(order.base_amount)+base_amount),
																  float(order.price),   # extreme price
																  first_price]  		# first price
			else:
				markets[(order.base_asset, order.quote_asset)] = [float(order.base_amount),
																  float(order.price),
																  float(order.price),  # extreme price
																  float(order.price)]  # first price
	return markets, g


def limit_order(r, assets):
	# from base to quote multiply by base_amount / quote_amount
	Order = namedtuple('order', ['type', 'base_amount', 'base_asset', 'quote_amount', 'quote_asset', 'price'])
	base_prec = assets.assets[r['sell_price']['base']['asset_id']].precision
	quote_prec = assets.assets[r['sell_price']['quote']['asset_id']].precision
	base_asset = assets.assets_name[r['sell_price']['base']['asset_id']]
	quote_asset = assets.assets_name[r['sell_price']['quote']['asset_id']]
	base_amount = int(r['sell_price']['base']['amount']) / 10**base_prec
	quote_amount = int(r['sell_price']['quote']['amount']) / 10**quote_prec
	price = base_amount / quote_amount
	base_amount = int(r['for_sale']) / 10 ** base_prec
	quote_amount = base_amount * (1/price)
	order = Order('SELL', base_amount, base_asset, quote_amount, quote_asset, price)
	return order


def price_range(p1, p2, steps=3):
	if p1 > p2:
		p = p2
		p2 = p1
		p1 = p
	s = [p1,p2]
	f = (p2-p1) / (steps-1)
	p = p2
	while True:
		p -= f
		if p <= p1:
			break
		s.append(p)
	s.sort()
	return s


if __name__ == '__main__':
	print(price_range(1,2,5))
