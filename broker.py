"""
Broker



"""

import trio
import pynng
import msgpack
import utils
from manualSIGNING import it

class Broker_client():

	def __init__(self):
		self.address = 'tcp://127.0.0.1:15001'
		self.sock = None
		self.msg_id = 0
		self.send_queue = []
		self.callbacks = {}


	async def comm_recv(self):
		while True:
			msg = await self.sock.arecv_msg()
			try:
				d = msgpack.loads(msg.bytes, raw=False)
			except Exception as err:
				print('> recv', err)
			print(f"> recv id: {d['id']}  msg: {d.__repr__()[:100]}")
			if d['id'] in self.callbacks:
				self.callbacks[d['id']](d)

	async def comm_send(self):
		while True:
			if self.send_queue:
				send = self.send_queue.pop(0)
				self.msg_id += 1
				msg = {'op':send[0], 'id': self.msg_id, 'query':send[1]}
				if send[2] is not None:
					self.callbacks[str(self.msg_id)] = send[2]
				await self.sock.asend(msgpack.packb(msg, use_bin_type=True))
				if 'dequeue' not in msg.__repr__()[:80]:
					print(f"< send id: {self.msg_id}  msg: {msg.__repr__()[:100]}")
				await trio.sleep(.001)
			else:
				await trio.sleep(.01)

	async def query(self, op, q, cb):
		self.send_queue.append([op, q, cb])

	def query_sync(self, op, q, cb):
		self.send_queue.append([op, q, cb])


	async def start(self):
		with pynng.Pair1(polyamorous=True) as sock:
			async with trio.open_nursery() as nur:
				self.sock = sock
				self.sock.dial(self.address)
				nur.start_soon(self.comm_send)
				nur.start_soon(self.comm_recv)


class Tasks():

	def __init__(self, broker):
		self.broker = broker
		self.tasks = {}
		self.refresh = 5
		self.task_pending = False
		self.tasks_todo = []

	async def get_tasks(self):
		'''
		query broker server's task queues for ops
		:return:
		'''
		def tasks(rtn):
			print(it('yellow', "broker.get_tasks"), rtn)
			self.tasks_todo.append(rtn)
		for t in self.tasks.keys():
			await self.broker.query('dequeue '+t, [], tasks)

	async def do_tasks(self):
		def done(task, task_name, result):
			print(it('yellow', "broker.do_tasks done"), task, task_name, result)
			self.broker.send_queue.append(['taskdone', [task, task_name, result], None])
		while self.tasks_todo:
			t = self.tasks_todo.pop(0)
			func = utils.partial(done, t['params'][1], t['task'])
			print(it('yellow', "broker.do_tasks"), t['task'], t['params'][0])
			await self.tasks[t['task']].start(t['params'][0], func)

	async def main(self):
		while True:
			if self.tasks_todo:
				await self.do_tasks()
			else:
				await self.get_tasks()
			await trio.sleep(1)

	async def start(self):
		async with trio.open_nursery() as nur:
			nur.start_soon(self.broker.start)
			nur.start_soon(self.main)


if __name__ == '__main__':
	pass
