"""
WORKER 1

Tasks: MarketOrderBook, ProfitPath

"""

import trio
import json
from collections import namedtuple
from broker import Broker_client, Tasks
from models import Settings
import utils
from manualSIGNING import it
from datetime import datetime
from os import remove
from time import time
from itertools import combinations
import networkx as nx

Mem = utils.Borg()

Order = namedtuple('order', ['type', 'base_amount', 'base_asset', 'quote_amount', 'quote_asset', 'price'])




class MarketOrderBook():
	def __init__(self, parent):
		self.broker = parent.broker
		self.assets = utils.Assets(parent.broker)
		self.markets = set()
		self.fileoutput = 'orderbook.csv'


	async def start(self, params, cb):
		self.markets = self.markets.union({(x[0], x[1]) for x in params[1]})
		self.num_responses = 0
		self.fileoutput = params[0]
		def response(rtn):
			print("get limit orders", rtn['query'])
			orders = {}  # [(p1, p2)]['sell'] = [orders]
			result = rtn['result']
			with open(self.fileoutput, 'a') as f:
				try:
					for r in result:
						if r['seller'] == Mem.settings.account_id:  # exclude own orders
							continue
						order = utils.limit_order(r, self.assets)
						pair = (order.base_asset, order.quote_asset)
						f.write(f"{order.type}, {order.quote_amount}, {order.quote_asset}, {order.base_amount}, {order.base_asset}, {order.base_amount/order.quote_amount} \n")
						if pair not in orders:
							orders[pair] = []
						orders[pair].append(order)
				except Exception as err:
					print("*************** error!!!!", err)
			self.num_responses += 1
			print()
			#cb(True)
		try:
			remove(self.fileoutput)
		except:
			pass
		with open(self.fileoutput, 'a') as f:
			f.write('type, from_amount, from_asset, to_amount, to_asset, factor \n')
		while True:
			for m in self.markets:
				self.assets.check_asset(asset_name=m[0])
				self.assets.check_asset(asset_name=m[1])
				await self.broker.query('bts_query', ['database', 'get_limit_orders', [m[0], m[1], 20]], response)
			with trio.move_on_after(30):
				while self.num_responses < len(self.markets):
					await trio.sleep(1)
					print("responses", self.num_responses, len(self.markets))
				break
			print(it('cyan', 'marketorderbook retry'))
			continue
		print(it('cyan', 'marketorderbook complete'))
		cb(True)


class ProfitPath():

	def __init__(self):
		self.orders_to_get = 4
		self.chain_lenght = 4  # max operations

	async def start(self, params, cb):
		print(it('cyan', 'profitpath.start'))
		markets, g = utils.read_orderbook_file(filename=params[0], assets=params[1], depth=self.chain_lenght)
		assets = params[1]

		# test of presence of an order to sell 120*60 zeph in change of 120 cny)
		#markets[('CNY', 'ZEPH')] = [120, 60]
		with open('profit_path.txt', 'a') as ftxt:
			ftxt.write('\n\n' + datetime.utcnow().isoformat() + '-' * 20 + '\n')
		ops = []
		for asset in assets:
			await trio.sleep(0.0001)
			try:
				paths = nx.all_simple_paths(g, source=asset[0], target=asset[0] + '*', cutoff=self.chain_lenght)
			except Exception as err:
				print("node not found", err)
				continue
			txt = ''
			for path in map(nx.utils.pairwise, paths):
				p1 = list(path)
				money = asset[1]
				money_efec = asset[1]
				lin = ''
				lin_efec = ''
				steps = []
				for m in p1:
					if '*' in m[0]:
						m = (m[0][:-1], m[1])
					elif '*' in m[1]:
						m = (m[0], m[1][:-1])
					if m not in markets:
						money = 0  # forces to don't write
						money_efec = 0
						break
					mkt = markets[m]
					premoney = money
					premoney_efec = money_efec
					if money_efec > mkt[0]:  # honor max offer
						money_efec = mkt[0] * mkt[1]
					else:
						money_efec *= mkt[1]
					money *= mkt[1]
					lin += f'{premoney}{m[0]} (max {mkt[0]}) buy {m[1]} @{mkt[1]} = {money}{m[1]} > '
					lin_efec += f'{premoney_efec}{m[0]} buy {m[1]} @{mkt[1]} = {money_efec}{m[1]} > '
					steps.append({'sell': [premoney, m[0]], 'purchase': [money, m[1]], 'price': mkt[2]})
				if money > asset[1]*asset[2]:
					ratio = (money-asset[1])/asset[1]
					perc = round(100*ratio,2)
					lin = f"profit: {money-asset[1]}{asset[0]} ({perc}%) >> \n" + lin
					lin_efec = f"efective profit: {money_efec/(1+ratio)}{asset[0]} > {money_efec}{asset[0]} >> \n" + lin_efec
					print(it('green', "potential profit!!! "+lin))
					txt += lin + "\n\n"
					txt += lin_efec + "\n\n\n"
					# correct ratio for efective gain
					for s in steps:
						s['sell'][0] *= (1+ratio)
						s['purchase'][0] *= (1+ratio)
					ops.append({'gain': (money_efec - (money_efec/(1+ratio)), asset[0]), 'steps_num': len(steps), 'ops': steps})
			if txt != '':
				with open('profit_path.txt', 'a') as ftxt:
					ftxt.write(txt)
		with open('profit_path.json', 'w') as f:
			f.write(json.dumps(ops))
		cb(True)
		print(it('cyan', 'profitpath.end'))


class Logic():

	def __init__(self, task_broker):
		self.task_broker = task_broker
		self.broker = task_broker.broker
		self.assets = utils.Assets(self.broker)
		self.task_broker.tasks = {'get_bts_orderbook': MarketOrderBook(parent=self),
								  'profitpath': ProfitPath()}
		self.last_orderbook = 0
		self.cycle = 60*15
		self.profitable_tokens = [["BTS", 10, 1],
									["CNY", 10, 1],
									["USD", 2, 1],
									["BRIDGE.BTC", 0.001, 1],
									["BRIDGE.USDT", 2, 1]
									]

	async def init(self):
		def subscription(rtn):
			print(it('purple', rtn))
		def fees(rtn):
			Mem.settings.fees = {'create': rtn['result'][0]['amount'], 'cancel': rtn['result'][1]['amount']}
		def account(rtn):
			Mem.settings.account_id = rtn['result'][0]['id']
			self.broker.query_sync('bts_query', ["database", "get_required_fees", [[["1", {"from": Mem.settings.account_id}], ["2", {"from": Mem.settings.account_id}]], "1.3.0"]], fees)
		await self.broker.query('bts_query', ['database', 'get_accounts', [[Mem.settings.account.get_secret_value()]]], account)
		await self.broker.query('subscribe get_bts_orderbook', [], subscription)
		await self.broker.query('subscribe profitpath', [], subscription)


	async def search_profitablepath(self):
		def end_task_profitpath(rtn):
			print("*********** end task profitpath", rtn)

		def end_task_orderbook(rtn):
			print("*********** end task orderbook", rtn)
			self.last_orderbook = time()
			self.broker.query_sync('enqueue profitpath', ['orderbook.csv', self.profitable_tokens], end_task_profitpath)

		# test task
		s = [
			{"BTS", "CNY", "USD", "EUR", "JPY", "ARS", "RUBLE", "GCNY", "CAD", "CHF", "SGD", "KRW", "BTC"},
			{"SPARKDEX.AUD", "STABLE.PHP", "SPARKDEX.EUR", "SPARKDEX.GBP", "SPARKDEX.HKD", "SPARKDEX.SGD", "ZEPH"},
			{"SPARKDEX.USD", "GDEX.USDT", "RUDEX.USDT", "ALGO.USDT"},
			{"BTC", "SPARKDEX.BTC", "RUDEX.BTC", "GDEX.BTC", "ALGO.BTC"},
			{"SPARKDEX.ETH", "RUDEX.ETH", "GDEX.ETH", "ALGO.ETH"},
			{"GDEX.EOS", "RUDEX.EOS"},
			{"SKULD", "URTHR", "VERTHANDI"},
			{x[0] for x in self.profitable_tokens}
			 ]
		tmp = set()
		for t in s[:4]:  # reduce markets
			tmp = tmp.union(t)
		self.combinable_markets = tmp

		#self.combinable_markets = {'BTS', 'CNY', 'USD'}
		self.markets = list(combinations(self.combinable_markets, 2))
		if time() - self.last_orderbook > self.cycle /2:
			await self.broker.query('enqueue get_bts_orderbook', ['orderbook.csv', self.markets], end_task_orderbook)
		else:
			await self.broker.query('enqueue profitpath', ['orderbook.csv', self.profitable_tokens], end_task_profitpath)

	async def main(self):
		await self.init()
		while True:
			await self.search_profitablepath()
			await trio.sleep(self.cycle)

	async def start(self):
		async with trio.open_nursery() as nur:
			nur.start_soon(self.task_broker.start)
			nur.start_soon(self.main)



def load_settings():
	import glob
	cfg_file = '.worker1.cfg'
	st = Settings(account='account', account_id='', wif='234324324', markets=['a', 'b'])
	if len(glob.glob(cfg_file)) == 0:
		with open(cfg_file, 'w') as f:
			f.write(st.json())
	else:
		with open(cfg_file, 'r') as f:
			txt = f.read()
		Mem.settings = Settings(**json.loads(txt))



if __name__ == '__main__':
	load_settings()
	main = Logic(task_broker=Tasks(broker=Broker_client()))
	trio.run(main.start)

