"""
Broker



"""

import trio
import pynng
from collections import OrderedDict
import utils
import models
import json
import msgpack
from trio_websocket import open_websocket_url
from manualSIGNING import it


class Bts_ws():
	def __init__(self):
		self.uri = "wss://btsfullnode.bangzi.info/ws"
		self.ws = None
		self.queue = []  # [["database", "get_dynamic_global_properties", []], callback]
		self.send_queue = []
		self.msg_id = 0
		self.callbacks = {}

	def add(self, query, cb_id, cb):
		self.queue.append([query, cb_id, cb])

	async def next_step(self):
		if len(self.queue) > 0:
			id = self.msg_id
			self.msg_id += 1
			step = self.queue.pop(0)
			func = utils.partial(step[2], step[1], step[0])
			self.callbacks[id] = func
			self.send_queue.append(json.dumps(OrderedDict({"method": "call", "params": step[0], "jsonrpc": "2.0", "id": id})))
			return True
		else:
			return False


	async def send(self):
		while True:
			if self.send_queue:
				msg = self.send_queue.pop(0)
				try:
					print("< bts_ws send ", msg.__repr__()[:100])
					await self.ws.send_message(msg)
				except Exception as err:
					print("* bts_ws send ", err)
					self.send_queue.append(msg)  # requeue
					break
			else:
				await self.next_step()
				await trio.sleep(0.001)
		print("exiting bts_ws.send")

	async def recv(self):
		while True:
			data = None
			try:
				msg = await self.ws.get_message()
			except Exception as err:
				print(it('red', "* bts_ws recv1 "), err)
				break
			try:
				data = json.loads(msg)
				cb = self.callbacks[data['id']]
				del self.callbacks[data['id']]
				await cb(data['result'])
				print("> bts_ws recv: ", cb, data['id'], data.__repr__()[:80])
			except Exception as err:
				if data:
					mod = models.bts_error_10(**data['error'])
					print(it('red', "* bts_ws recv2 "), mod.code, mod.message, mod.data.name, mod.data.stack[0].context)
				print(it('red', "* bts_ws recv2 "), err)
		print("exiting bts_ws.recv")

	async def start(self):
		while True:
			try:
				async with open_websocket_url(self.uri) as ws:
					self.ws = ws
					async with trio.open_nursery() as nur:
						nur.start_soon(self.send)
						nur.start_soon(self.recv)
					print("exiting nursery")
			except Exception as err:
				print("!!!! connection error", err)
				await trio.sleep(2)
			print("------ bts_ws reconnecting")


class Broker_server():

	def __init__(self, bts_ws):
		self.address = 'tcp://127.0.0.1:15001'
		self.sock = None
		self.bts_ws = bts_ws
		self.callbacks = {}
		self.send_queue = []
		self.task_queues = {}
		self.subscriptions = {}

	async def do_ops(self, d, cb_id, pipe):
		self.callbacks[cb_id] = pipe
		if d['op'] == 'bts_query':
			self.bts_ws.add(d['query'], cb_id, self.ops_done)
		elif d['op'][:7] == 'enqueue':
			task = d['op'][8:]
			if task not in self.task_queues:
				self.task_queues[task] = []
			self.task_queues[task].append([d['query'], cb_id])
			await self.comm_send_subscribers(task, {'op': 'taskenqueued'})
		elif d['op'][:7] == 'dequeue':
			task = d['op'][8:]
			if task in self.task_queues and self.task_queues[task]:
				while self.task_queues[task]:
					t = self.task_queues[task].pop(0)
					self.send_queue.append([cb_id, msgpack.packb({'id': cb_id.split('@')[0], 'task': task, 'params': t}, use_bin_type=True)])
		elif d['op'][:8] == 'taskdone':
			task = d['query'][1]
			result = d['query'][2]
			msg = msgpack.packb({'id': d['query'][0].split('@')[0], 'task': task, 'msg': 'taskdone', 'result': result}, use_bin_type=True)
			self.send_queue.append([d['query'][0], msg])
			await self.comm_send_subscribers(task, {'op': 'taskdone', 'result': result})
		elif d['op'][:9] == 'subscribe':
			task = d['op'][10:]
			if task not in self.subscriptions:
				self.subscriptions[task] = []
			if pipe not in [x[1] for x in self.subscriptions[task]]:
				self.subscriptions[task].append([cb_id, pipe])

	async def ops_done(self, cb_id, query, rtn):
		print("ops_done", cb_id, query)
		self.send_queue.append([cb_id, msgpack.packb({'id': cb_id.split('@')[0], 'query': query, 'result': rtn}, use_bin_type=True)])


	async def comm_recv(self):
		while True:
			msg = await self.sock.arecv_msg()
			d = msgpack.loads(msg.bytes, raw=False)
			cb_id = str(d['id'])+'@'+str(msg.pipe.remote_address)
			await self.do_ops(d, cb_id, msg.pipe)
			if 'dequeue' not in d.__repr__()[:80]:
				print(f'{msg.pipe.remote_address} says {d}')

	async def comm_send(self):
		while True:
			if len(self.send_queue) > 0:
				q = self.send_queue.pop(0)
				if q[0] in self.callbacks:
					await self.callbacks[q[0]].asend(q[1])
					del self.callbacks[q[0]]
					print("< client comm_send", q.__repr__()[:80])
			else:
				await trio.sleep(.01)

	async def comm_send_subscribers(self, task, d):
		if task in self.subscriptions:
			for cb_id, pipe in self.subscriptions[task]:
				d.update({'id': cb_id.split('@')[0], 'task': task})
				msg = msgpack.packb(d, use_bin_type=True)
				await pipe.asend(msg)

	async def comm_send_broadcast(self, msg):
		for pipe in self.sock.pipes:
			await pipe.asend(msg.encode())

	async def start(self):
		with pynng.Pair1(polyamorous=True) as sock:
			async with trio.open_nursery() as nur:
				self.sock = sock
				def connect(pipe):
					print(f"got connection from {pipe.remote_address}")
				def remove(pipe):
					print(f"disconnected {pipe.remote_address}")
				sock.add_pre_pipe_connect_cb(connect)
				sock.add_post_pipe_remove_cb(remove)
				sock.listen(self.address)
				nur.start_soon(self.comm_send)
				nur.start_soon(self.comm_recv)
				nur.start_soon(self.bts_ws.start)



if __name__ == '__main__':
	comm = Broker_server(bts_ws=Bts_ws())
	trio.run(comm.start)

