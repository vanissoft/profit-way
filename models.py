
from datetime import datetime
from typing import List, Dict, Set
from pydantic import BaseModel, SecretStr, BaseSettings




class Amount(BaseModel):
	amount: int = 0
	asset_id: str

class AssetDescription(BaseModel):
	main: str = ''
	shortname: str
	market: str


class AssetOptions(BaseModel):
	max_supply: str
	market_fee_percent: int
	max_market_fee: str
	issuer_permissions: int = 0
	flags: int = 0
	core_exchange_rate: Dict[str, Amount]
	whitelist_authorities: List = []
	blacklist_authorities: List = []
	whitelist_markets: List = []
	blacklist_markets: List = []
	description: str = ''
	extensions: Dict


class Asset(BaseModel):
	id: str
	symbol: str
	precision: int
	issuer: str
	options: AssetOptions
	dynamic_asset_data_id: str


class AccountAuthority(BaseModel):
	weight_threshold: int
	account_auths: List
	key_auths: List
	address_auths: List

class AccountOptions(BaseModel):
	memo_key: str
	voting_account: str
	num_witness: int
	num_committee: int
	votes: List
	extensions: List

class Account(BaseModel):
	id: str
	membership_expiration_date: datetime
	registrar: str
	referrer: str
	lifetime_referrer: str
	network_fee_percentage: int
	lifetime_referrer_fee_percentage: int
	referrer_rewards_percentage: int
	name: str
	owner: AccountAuthority
	active: AccountAuthority
	options: AccountOptions
	statistics: str
	whitelisting_accounts: List = []
	blacklisting_accounts: List = []
	whitelisted_accounts: List = []
	blacklisted_accounts: List = []
	owner_special_authority: List = [0, {}]
	active_special_authority: List = [0, {}]
	top_n_control_flags: int = 0

class OrderbookBid(BaseModel):
	price: str
	quote: str
	base: str

class OrderbookAsk(BaseModel):
	price: str
	quote: str
	base: str

class OrderBook(BaseModel):
	base: str
	quote: str
	bids: List[OrderbookBid]
	asks: List[OrderbookAsk]



class Asset(BaseModel):
	id: str
	symbol: str
	precision: int
	dynamic_asset_data_id: str = None
	bitasset_data_id: str = None


class BotInstance(BaseModel):
	markets: List = []
	amount: set = {}
	orders: int = 1
	expiration: int = 30
	output: str = ''
	price_minimums: Dict = {}

class Settings(BaseSettings):
	account: SecretStr
	account_id: SecretStr
	fees: Dict = {}
	block: Dict = {}
	wif: SecretStr
	markets = ['BRIDGE.ABET', 'BRIDGE.BTC']
	assets: Dict = {}



class bts_error_10_data_stack(BaseModel):
	context: Dict = {}

class bts_error_10_data(BaseModel):
	code: int
	name: str
	message: str
	stack: List[bts_error_10_data_stack]
bts_error_10_data.update_forward_refs()

class bts_error_10(BaseModel):
	code: str
	message: str
	data: bts_error_10_data
bts_error_10.update_forward_refs()

